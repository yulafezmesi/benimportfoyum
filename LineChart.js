import { Line, mixins } from "vue-chartjs";
const { reactiveProp } = mixins;
const { reactiveData } = mixins;

export default {
  extends: Line,
  mixins: [reactiveProp],
  data() {
    return {
      options: {
        hoverBorderWidth: 20,
        maintainAspectRatio: false,
        scales: {
          yAxes: [
            {
              ticks: {
                // Include a dollar sign in the ticks
                callback: function (value, index, values) {
                  return "%" + value;
                }
              }
            }
          ]
        }
      }
    };
  },
  mounted() {
    // this.chartData is created in the mixin.
    // If you want to pass options please create a local options object
  },
  methods: {
    update(val) {

      this.renderChart(this.chartData, this.options);
    }
  }
};
