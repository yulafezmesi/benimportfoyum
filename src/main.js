import 'babel-polyfill'
import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
var SocialSharing = require("vue-social-sharing");
Vue.use(SocialSharing);
Vue.config.productionTip = false;

new Vue({
  vuetify,
  render: h => h(App)
}).$mount("#app");
